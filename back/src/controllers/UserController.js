// Importações
const { response } = require('express');
const { validationResult } = require('express-validator');
const User = require('../models/User');
const Auth = require('../config/auth');

// Registro de usuário
const create = async (req, res) => {
    try {
        validationResult(req).throw();
        const { password } = req.body;
        const hashAndSalt = Auth.generatePassword(password);
        const salt = hashAndSalt.salt;
        const hash = hashAndSalt.hash;
        const newUserData = {
            email: req.body.email,
            name: req.body.name,
            birth_date: req.body.birth_date,
            username: req.body.username,
            phone_number: req.body.phone_number,
            cpf: req.body.cpf,
            hash: hash,
            salt: salt
        }
        const user = await User.create(newUserData);
        return res.status(201).json({ message: "Usuário cadastrado.", user: user });
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}

// Mostrar todos os usuários
const index = async (req, res) => {
    try {
        const users = await User.findAll(req.body);
        return res.status(200).json({ users });
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}

// Mostrar usuário específico
const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({ user });
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

// Atualizar dados do usuário
const update = async (req, res) => {
    const { id } = req.params;
    try {
        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

// Deletar um usuário
const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Usuário deletado.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Usuário não encontrado.");
    }
}

// Exportação das funções
module.exports = {
    index,
    show,
    create,
    update,
    destroy
}