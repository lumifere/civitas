// Importações
const { response } = require('express');
const { validationResult } = require('express-validator');
const Order = require('../models/Order');
const User = require('../models/User');

// Registro de pedido
const create = async (req, res) => {
    try {
        validationResult(req).throw();
        const order = await Order.create(req.body);
        return res.status(201).json({ message: "Pedido criado.", order: order });
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}

// Mostrar todos os pedidos
const index = async (req, res) => {
    try {
        const orders = await Order.findAll(req.body);
        return res.status(200).json({ orders });
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}

// Mostrar pedido específico
const show = async (req, res) => {
    const { id } = req.params;
    try {
        const order = await Order.findByPk(id);
        return res.status(200).json({ order });
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}

// Atualizar um pedido
const update = async (req, res) => {
    const { id } = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await Order.update(req.body, { where: { id: id } });
        if (updated) {
            const order = await Order.findByPk(id);
            return res.status(200).send(order);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Pedido não encontrado.");
    }
}

// Remover um pedido
const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await Order.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Pedido deletado.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Pedido não encontrado.");
    }
}

// Funções de relação com a entidade Usuário

// Adicionar relação de 1-n (Usuário-Pedidos)
const addRelationUser = async (req, res) => {
    const { id } = req.params;
    try {
        const order = await Order.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await order.setUser(user);
        return res.status(200).json(order);
    } catch (err) {
        return res.status(500).json({ err });
    }
}

// Remover relação entre Usuário e Pedido
const removeRelationUser = async (req, res) => {
    const { id } = req.params;
    try {
        const order = await Order.findByPk(id);
        await order.setUser(null);
        return res.status(200).json(order);
    } catch (err) {
        return res.status(500).json({ err });
    }
}

// Exportação das funções
module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationUser,
    removeRelationUser
}