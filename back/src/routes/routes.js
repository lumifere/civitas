// Importações
const { Router } = require('express');
const validator = require("../config/validator");
const passport = require("passport");
const UserController = require('../controllers/UserController');
const OrderController = require('../controllers/OrderController');
const AuthController = require('../controllers/AuthController');

const router = Router();

// Rota para autenticação via JWT
router.use("/private", passport.authenticate('jwt', { session: false }));

// Rotas para autenticação de Login e mostrar informações
router.post('/login', AuthController.login);
router.get('/private/getDetails', AuthController.getDetails);

// Rotas para a entidade Usuário
router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', validator.validationUser('create'), UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

// Rotas para a entidade Pedido
router.get('/orders', OrderController.index);
router.get('/orders/:id', OrderController.show);
router.post('/orders', validator.validationOrder('create'), OrderController.create);
router.put('/orders/:id', OrderController.update);
router.delete('/orders/:id', OrderController.destroy);

// Rotas para adicionar e remover a ligação entre Usuário e Pedido
router.put('/ordersaddrole/:id', OrderController.addRelationUser);
router.delete('/ordersremoverole/:id', OrderController.removeRelationUser);

module.exports = router;