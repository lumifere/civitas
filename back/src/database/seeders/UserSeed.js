const User = require("../../models/User");
const faker = require('faker-br');

const seedUser = async function () {
    try {
        await User.sync({ force: true });

        for (let i = 0; i < 40; i++) {
            await User.create({
                name: faker.name.findName(),
                username: faker.internet.userName(),
                email: faker.internet.email(),
                birth_date: faker.date.between('1970-01-01', '2004-01-01'),
                phone_number: faker.phone.phoneNumber('+55 ## 9#### ####'),
                cpf: faker.br.cpf()
            });
        }
    } catch (err) {
        console.log(err);
    }
}

module.exports = seedUser;