require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');
const seedUser = require('./UserSeed');
const seedOrder = require('./OrderSeed');

(async () => {
  try {
    //await seedModel();
    await seedUser();
    await seedOrder();

  } catch (err) { console.log(err) }
})();