const Order = require("../../models/Order");
const User = require("../../models/User");
const faker = require('faker-br');

const seedOrder = async function () {
    try {
        await Order.sync({ force: true });

        for (let i = 0; i < 40; i++) {
            let order = await Order.create({
                product: faker.commerce.productName(),
                amount: faker.random.number({ min: 1, max: 10 }),
                price: faker.commerce.price(30, 800, 2, 'R$ '),
                payment_method: faker.finance.transactionType(),
                date: faker.date.recent(30),
                gift: faker.random.arrayElement(['Sim', 'Não'])
            });
            let user = await User.findByPk(faker.random.number({ min: 1, max: 25 }));
            order.setUser(user);
        }

    } catch (err) {
        console.log(err);
    }
}

module.exports = seedOrder;