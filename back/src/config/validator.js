const { body } = require("express-validator");

const validationUser = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('name').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 1 }).withMessage('Por favor, preencha o campo.'),
                body('username').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 5 }).withMessage('Digite um usuário com no mínimo 5 caracteres.'),
                body('email').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 1 }).isEmail().withMessage('Por favor, preencha o campo.'),
                body('birth_date').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 10, max: 10 }).isDate().withMessage('Insira uma data no formato AAAA/MM/DD.'),
                body('phone_number').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 10 }).withMessage('Insira um número de telefone com DDD.'),
                body('cpf').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 11, max: 11 }).withMessage('Insira um CPF válido.')
            ]
        }
    }
}

const validationOrder = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('product').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 1 }).withMessage('Por favor, preencha o campo.'),
                body('amount').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 1 }).withMessage('Insira uma quantidade válida.'),
                body('price').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 0 }).isCurrency().withMessage('Insira um valor válido.'),
                body('payment_method').exists().withMessage("Esse campo não pode estar vazio").isArray(['MasterCard', 'Visa', 'Pix']).withMessage('Aceitamos apenas "MasterCard", "Visa" e "Pix". Insira uma das opções.'),
                body('date').exists().withMessage("Esse campo não pode estar vazio").isLength({ min: 10, max: 10 }).isDate().withMessage('Insira uma data no formato AAAA/MM/DD.'),
                body('gift').exists().withMessage("Esse campo não pode estar vazio").isArray(['Sim', 'Não']).withMessage('Insira "Sim" ou "Não".'),
            ]
        }
    }
}

module.exports = {
    validationUser,
    validationOrder
}