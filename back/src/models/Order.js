// Importações
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Determinação dos campos da entidade Pedido
const Order = sequelize.define('Order', {
    product: {
        type: DataTypes.STRING,
        allowNull: false
    },

    amount: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    price: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    payment_method: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    gift: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps: false
});

// Determinação da relação entre modelos
Order.associate = function (models) {
    Order.belongsTo(models.User, { constraints: false });
}

// Exportação do modelo
module.exports = Order;