// Importações
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Determinação dos campos da entidade Usuário
const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    username: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hash: {
        type: DataTypes.STRING
    },

    salt: {
        type: DataTypes.STRING
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    birth_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});

// Determinação da relação entre modelos
User.associate = function (models) {
    User.hasMany(models.Order, { constraints: false });
}

// Exportação do modelo
module.exports = User;