const express = require('express');
require('./src/config/dotenv')();
require('./src/config/sequelize');

//const cors = require('cors');
const app = express();
const port = process.env.PORT;
const routes = require('./src/routes/routes');

const passport = require("passport");
require("./src/middlewares/jwtPassport")(passport);
app.use(passport.initialize());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(routes);

app.listen(port, () => {
  console.log(`${process.env.APP_NAME} app listening at http://localhost:${port}`);
});  